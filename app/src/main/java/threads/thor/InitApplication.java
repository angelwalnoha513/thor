package threads.thor;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationChannelGroup;
import android.app.NotificationManager;
import android.content.Context;

import androidx.annotation.NonNull;

import com.google.android.material.color.DynamicColors;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

import net.luminis.quic.QuicConnection;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import threads.LogUtils;
import threads.lite.IPFS;
import threads.lite.cid.PeerId;
import threads.thor.core.Content;
import threads.thor.core.DOCS;
import threads.thor.core.pages.PAGES;
import threads.thor.core.pages.Page;
import threads.thor.utils.AdBlocker;

public class InitApplication extends Application {

    public static final String TIME_TAG = "TIME_TAG";
    public static final String STORAGE_CHANNEL_ID = "STORAGE_CHANNEL_ID";
    public static final String STORAGE_GROUP_ID = "STORAGE_GROUP_ID";
    private static final String TAG = InitApplication.class.getSimpleName();
    private final Gson gson = new Gson();

    private void createStorageChannel(@NonNull Context context) {

        try {
            CharSequence name = context.getString(R.string.storage_channel_name);
            String description = context.getString(R.string.storage_channel_description);
            NotificationChannel mChannel = new NotificationChannel(
                    STORAGE_CHANNEL_ID, name, NotificationManager.IMPORTANCE_LOW);
            mChannel.setDescription(description);

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(
                    Context.NOTIFICATION_SERVICE);

            notificationManager.createNotificationChannelGroup(
                    new NotificationChannelGroup(STORAGE_GROUP_ID, name));

            notificationManager.createNotificationChannel(mChannel);

        } catch (Throwable throwable) {
            threads.lite.LogUtils.error(TAG, throwable);
        }

    }

    @Override
    public void onCreate() {
        super.onCreate();

        DynamicColors.applyToActivitiesIfAvailable(this);

        long start = System.currentTimeMillis();

        createStorageChannel(getApplicationContext());

        AdBlocker.init(getApplicationContext());


        LogUtils.info(TIME_TAG, "InitApplication after add blocker [" +
                (System.currentTimeMillis() - start) + "]...");
        try {
            IPFS ipfs = IPFS.getInstance(getApplicationContext());
            ipfs.setPusher(this::onMessageReceived);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

        LogUtils.info(TIME_TAG, "InitApplication after starting ipfs [" +
                (System.currentTimeMillis() - start) + "]...");


    }

    @SuppressWarnings("UnstableApiUsage")
    public void onMessageReceived(@NonNull QuicConnection conn,
                                  @NonNull String content) {

        try {
            Type hashMap = new TypeToken<HashMap<String, String>>() {
            }.getType();

            Objects.requireNonNull(conn);
            IPFS ipfs = IPFS.getInstance(getApplicationContext());

            Objects.requireNonNull(content);
            Map<String, String> data = gson.fromJson(content, hashMap);

            LogUtils.debug(TAG, "Push Message : " + data.toString());


            String ipns = data.get(Content.IPNS);
            Objects.requireNonNull(ipns);
            String pid = data.get(Content.PID);
            Objects.requireNonNull(pid);
            String seq = data.get(Content.SEQ);
            Objects.requireNonNull(seq);

            PeerId peerId = PeerId.fromBase58(pid);
            long sequence = Long.parseLong(seq);
            if (sequence >= 0) {
                if (ipfs.isValidCID(ipns)) {
                    PAGES pages = PAGES.getInstance(getApplicationContext());
                    Page page = pages.createPage(peerId.toBase58());
                    page.setContent(ipns);
                    page.setSequence(sequence);
                    pages.storePage(page);
                }
            }

            DOCS.getInstance(getApplicationContext()).addResolves(peerId, ipns);

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }
}
